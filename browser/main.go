package browser

import (
	"fmt"
	"log"
	"os"
	"os/exec"
	"runtime"
)

func OpenUrl(url string) {

	if os.Getenv("KIONCTL_OFFLINE_DEV") == "1" {
		fmt.Print("!! Replacing AWS login url with example.com because offline dev mode is activated !!\n\n")
		url = "https://example.com"
	}

	openTool := ""
	if runtime.GOOS == "linux" {
		openTool = "xdg-open"
	} else if runtime.GOOS == "darwin" {
		openTool = "open"
	} else {
		fmt.Println("\nThis OS not supported for automatic URL opening. Please copy and paste the following")
		fmt.Println("URL into your browser:")
		fmt.Print("\n*************************************\n\n")
		fmt.Println(url)
		fmt.Print("\n*************************************\n\n")
		return
	}

	cmd := exec.Command(openTool, url)
	output, err := cmd.CombinedOutput()
	if err != nil {
		log.Fatal(err)
	}
	fmt.Printf("Opening url in browser\n%s\n", string(output))
}
