package prompt

import (
	"github.com/AlecAivazis/survey/v2"

	"gitlab.com/benyanke/kionctl/kion"
	"gitlab.com/benyanke/kionctl/kion/api"
)

type Account = api.Account
type AccessRole = api.AccessRole
type Project = api.Project
type ApiClient = kion.Client
type Credential = api.Credential

// Interactively prompts for project, returning the user selected project struct
func ForProject(kionClient ApiClient) (Project, error) {

	projects, _ := kionClient.GetAllAccountInformation()

	projectStrings := []string{}
	for _, project := range projects {
		projectStrings = append(projectStrings, project.Name)
	}

	// the questions to ask
	var qs = []*survey.Question{
		{
			Name: "project",
			Prompt: &survey.Select{
				Message: "Choose a project:",
				Options: projectStrings,
				// Default to the most commonly used project
				Default: projectStrings[0],
			},
		},
	}

	// the answers will be written to this struct
	answers := struct {
		ProjectName string `survey:"project"`
	}{}

	// perform the questions
	err := survey.Ask(qs, &answers)
	if err != nil {
		return Project{}, err
	}

	// Converts the project name into the project struct, and returns it
	return kionClient.GetProjectByProjectName(answers.ProjectName), nil
}

// Interactively prompts for account, returning the user selected project struct
func ForAccount(kionClient ApiClient, project Project) (Account, error) {

	accountStrings := []string{}
	for _, accounts := range project.Accounts {
		accountStrings = append(accountStrings, accounts.Name)
	}

	// the questions to ask
	var qs = []*survey.Question{
		{
			Name: "account",
			Prompt: &survey.Select{
				Message: "Choose an account:",
				Options: accountStrings,
				// Default to the most commonly used project
				Default: accountStrings[0],
			},
		},
	}

	// the answers will be written to this struct
	answers := struct {
		AccountName string `survey:"account"`
	}{}

	// perform the questions
	err := survey.Ask(qs, &answers)
	if err != nil {
		return Account{}, err
	}

	// Converts the project name into the project struct, and returns it
	return kionClient.GetAccountByAccountName(project, answers.AccountName), nil
}

// Interactively prompts for project, returning the user selected project struct
func ForRole(kionClient ApiClient, account Account) (AccessRole, error) {

	roleStrings := []string{}
	for _, role := range account.AccessRoles {
		roleStrings = append(roleStrings, role.Name)
	}

	// the questions to ask
	var qs = []*survey.Question{
		{
			Name: "role",
			Prompt: &survey.Select{
				Message: "Choose an account role:",
				Options: roleStrings,
				// Default to the most commonly used project
				Default: roleStrings[0],
			},
		},
	}

	// the answers will be written to this struct
	answers := struct {
		RoleName string `survey:"role"`
	}{}

	// perform the questions
	err := survey.Ask(qs, &answers)
	if err != nil {
		return AccessRole{}, err
	}

	// Converts the project name into the project struct, and returns it
	return kionClient.GetRoleByRoleName(account, answers.RoleName), nil
}

// Interactively prompts for an arbitrary string
func ForEnumString(message string, options []string) (string, error) {

	// the questions to ask
	var qs = []*survey.Question{
		{
			Name: "out",
			Prompt: &survey.Select{
				Message: message,
				Options: options,
				Default: options[0],
			},
		},
	}

	// the answers will be written to this struct
	answers := struct {
		Out string `survey:"out"`
	}{}

	// perform the questions
	err := survey.Ask(qs, &answers)
	if err != nil {
		return "", err
	}

	// Converts the project name into the project struct, and returns it
	return answers.Out, nil
}
