package aws

import (
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/credentials"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/sts"
	"gitlab.com/benyanke/kionctl/kion/api"
	"os"
)

type Credential = api.Credential

// Given a set of credentials, an account number, and a role name to assume, this function
// returns a new set of credentials with the given role, or an error if it could not be assumed
func AssumeNamedRole(creds Credential, accountNumber string, roleNameToAssume string) (Credential, error) {

	// In debugging mode, just return back the mocked creds
	if os.Getenv("KIONCTL_OFFLINE_DEV") == "1" {
		return creds, nil
	}

	// Make credential object for AWS SDK with the passed in credentials
	awsCredStruct := credentials.NewStaticCredentials(creds.AccessKey, creds.SecretAccessKey, creds.SessionToken)

	// Initialize an AWS sdk session with these creds
	sess, err := session.NewSession(&aws.Config{
		Credentials: awsCredStruct,
	})

	if err != nil {
		//fmt.Println("NewSession Error", err)
		return Credential{}, err
	}

	// Create a STS client
	svc := sts.New(sess)

	roleToAssumeArn := "arn:aws:iam::" + accountNumber + ":role/" + roleNameToAssume

	// Note - this session (seemingly) can be any string. Setting it to the role name, lacking anything better
	sessionName := roleNameToAssume

	result, err := svc.AssumeRole(&sts.AssumeRoleInput{
		RoleArn:         &roleToAssumeArn,
		RoleSessionName: &sessionName,
	})

	if err != nil {
		//fmt.Println("AssumeRole Error", err)
		return Credential{}, err
	}

	// Convert the cred bundle back to our format to return
	returnedCredBundle := Credential{
		AccessKey:       *result.Credentials.AccessKeyId,
		SecretAccessKey: *result.Credentials.SecretAccessKey,
		SessionToken:    *result.Credentials.SessionToken,
	}

	return returnedCredBundle, nil

}
