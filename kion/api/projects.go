package api

import (
	"encoding/json"
	"net/http"
)

// projects list struct - only used to parse out the http response
type projects struct {
	//status int       `json:"status"`
	List []Project `json:"data"`
}

type Project struct {
	ID       int       `json:"id"`
	Name     string    `json:"name"`
	Accounts []Account `json:"accounts"`
	// Used by stats client-side, not returned by the server
	StatsCounter int
}

// Get projects
func (k Client) GetProjects() ([]Project, error) {

	url := k.BaseUrl + "/api/v3/project"

	request, err := http.NewRequest("GET", url, nil)
	if err != nil {
		return nil, err
	}

	body, err := k.executeRequestAndCheckResponse(request)
	if err != nil {
		return nil, err
	}

	var projects projects
	json.Unmarshal(body, &projects)
	return projects.List, nil

}
