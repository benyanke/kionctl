package api

import (
	"embed"
	"errors"
	"io"
	"net/http"
	"os"
	"time"
)

// This file contains common functions and definitions for the raw api connection

//go:embed offline_dev_mocks/*
var mock_data embed.FS

type Client struct {
	BaseUrl     string
	ApiToken    string
	HttpTimeout int
	Debug       bool
}

// Sets the headers used for every request
func (k Client) setStandardHeaders(request *http.Request) {

	bearer := "Bearer " + k.ApiToken

	request.Header.Set("Authorization", bearer)
	request.Header.Set("Accept", "application/json")
	request.Header.Set("Content-Type", "application/json")

}

// Executes the http request and checks response for basic http errors
func (k Client) executeRequestAndCheckResponse(request *http.Request) ([]byte, error) {

	if os.Getenv("KIONCTL_OFFLINE_DEV") == "1" {
		return k.handleMocks(request)
	}

	// Set the standard set of headers, including auth
	k.setStandardHeaders(request)

	// Send request using http Client
	client := &http.Client{
		Timeout: time.Duration(k.HttpTimeout) * time.Second,
	}
	resp, err := client.Do(request)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()

	// Often an unauthorized response indicates an API key expiring or otherwise being invalid - let's help the users a bit
	if resp.StatusCode != http.StatusOK {
		return nil, errors.New("HTTP response was unauthorized. Perhaps API key problem or bad input option? URL requested " + string(request.URL.String()))
	}

	// Catch more generic errors
	if resp.StatusCode != http.StatusOK {
		return nil, errors.New("HTTP response was '" + resp.Status + "', not 200. URL requested " + string(request.URL.String()))
	}

	body, err := io.ReadAll(resp.Body)
	if err != nil {
		return nil, err
	}

	return body, nil
}

// Handles the content mocking for offline dev mode, replacing the HTTP request
// client with a mocked one which doesn't go out to the internet.
//
// mock_data is a virtual filesystem (see the 'go embed' directive at the top of the file)
// used to store the mock responses. since ReadFile returns the same type (io reader) as executeRequestAndCheckResponse
// normally does, it can simply return back in place. You don't need to worry about HTTP
// response code mocking or any related concerns, because executeRequestAndCheckResponse encapsulates
// all of that.
func (k Client) handleMocks(request *http.Request) ([]byte, error) {

	if os.Getenv("KIONCTL_OFFLINE_DEV") != "1" {
		panic("You shouldn't be calling this when you're not in offline dev mode")
	}

	path := request.URL.EscapedPath()
	method := request.Method
	methodPathSelector := method + " " + path

	// For debugging
	//fmt.Println("OUTBOUND HTTP CALL CAUGHT AND MOCKED - " + selector)

	switch methodPathSelector {
	// Map URLs to their mock returns
	case "GET /api/v3/me/cloud-access-role":
		return mock_data.ReadFile("offline_dev_mocks/cloud_access_roles.json")

	case "GET /api/v3/project":
		return mock_data.ReadFile("offline_dev_mocks/projects.json")

	case "GET /api/v3/account":
		return mock_data.ReadFile("offline_dev_mocks/accounts.json")

	case "POST /api/v3/temporary-credentials":
		return mock_data.ReadFile("offline_dev_mocks/temporary-credentials.json")

	default:
		panic("Unexpected mock url : " + methodPathSelector)
	}

}
