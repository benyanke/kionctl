package api

import (
	"encoding/json"
	"net/http"
)

// access role list struct - only used to parse out the http response
type accessRoles struct {
	Status int          `json:"status"`
	List   []AccessRole `json:"data"`
}

type AccessRole struct {
	ID   int    `json:"id"`
	Name string `json:"name"`
	//ProjectID           int    `json:"project_id"`
	AwsIamRoleName      string `json:"aws_iam_role_name"`
	WebAccess           bool   `json:"web_access"`
	ShortTermAccessKeys bool   `json:"short_term_access_keys"`
	//LongTermAccessKeys  bool   `json:"long_term_access_keys"`
	//	FutureAccounts      bool   `json:"future_accounts"`
	//AwsIamPath string `json:"aws_iam_path"`
	//	ApplyToAllAccounts  bool   `json:"apply_to_all_accounts"`
	//	CreatedAt           struct {
	//		Time  time.Time `json:"Time"`
	//		Valid bool      `json:"Valid"`
	//	} `json:"created_at"`
	//	UpdatedAt struct {
	//		Time  time.Time `json:"Time"`
	//		Valid bool      `json:"Valid"`
	//	} `json:"updated_at"`
	//	DeletedAt struct {
	//		Time  time.Time `json:"Time"`
	//		Valid bool      `json:"Valid"`
	//	} `json:"deleted_at"`
	AccountID int `json:"account_id"`
	//AccountType   string `json:"account_type"`
	//AccountNumber string `json:"account_number"`
	//CloudAccessRoleType string `json:"cloud_access_role_type"`
	// Used by stats client-side, not returned by the server
	StatsCounter int
}

// Get access roles
func (k Client) GetAccessRoles() ([]AccessRole, error) {

	url := k.BaseUrl + "/api/v3/me/cloud-access-role"

	request, err := http.NewRequest("GET", url, nil)
	if err != nil {
		return nil, err
	}

	body, err := k.executeRequestAndCheckResponse(request)
	if err != nil {
		return nil, err
	}

	var accessRoles accessRoles
	json.Unmarshal(body, &accessRoles)

	return accessRoles.List, nil

}
