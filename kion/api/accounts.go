package api

import (
	"encoding/json"
	"net/http"
)

// Account List struct - only used to parse out the http response
type accounts struct {
	//status int       `json:"status"`
	List []Account `json:"data"`
}

type Account struct {
	ID     int    `json:"id"`
	Number string `json:"account_number"`
	Name   string `json:"account_name"`
	//AccountEmail  string `json:"account_email"`
	//LinkedRole    string `json:"linked_role"`
	ProjectID int `json:"project_id"`
	//PayerID                   int    `json:"payer_id"`
	//CreatedAt                 string `json:"created_at"`
	//AccountTypeID             int    `json:"account_type_id"`
	//StartDatecode             string `json:"start_datecode"`
	//SkipAccessChecking        bool   `json:"skip_access_checking"`
	//UseOrgAccountInfo         bool   `json:"use_org_account_info"`
	//LinkedAccountNumber       string `json:"linked_account_number"`
	//DeletedAt                 string `json:"deleted_at"`
	//IncludeLinkedAccountSpend bool   `json:"include_linked_account_spend"`

	// This field not parsed from the API, but populated later
	AccessRoles []AccessRole `json:"access_roles"`

	// Used by stats client-side, not returned by the server
	StatsCounter int
}

// Get accounts
func (k Client) GetAccounts() ([]Account, error) {

	url := k.BaseUrl + "/api/v3/account"

	request, err := http.NewRequest("GET", url, nil)
	if err != nil {
		return nil, err
	}

	body, err := k.executeRequestAndCheckResponse(request)
	if err != nil {
		return nil, err
	}

	var accounts accounts
	json.Unmarshal(body, &accounts)

	return accounts.List, nil
}
