package cache

// This package is a thin abstraction around github.com/peterbourgon/diskv to provide more testability
// and flexibility in the future

// TODO - add support for item expiry by timestamp

import (
	"os"
	"strconv"

	"github.com/peterbourgon/diskv/v3"
)

var KEY_NAMESPACE = ""

// Cache keys - these are included and used in consumers to avoid magic strings
const KEY_STATISTICS = "credential_fetch_statistics"
const KEY_ACCOUNTS = "account_list"
const KEY_PROJECTS_RAW = "project_raw_list"
const KEY_ACCOUNTS_RAW = "account_raw_list"
const KEY_ROLES_RAW = "access_roles_raw_list"
const KEY_LAST_RUNTIME = "last_runtime"

type Cache struct {
	Initialized bool
	CacheDir    string
	CacheSize   int
	backend     *diskv.Diskv
}

// TODO - implement this
func (c Cache) validateConfig() error {
	// TODO - add checks
	return nil
}

func (c *Cache) Init() {
	c.validateConfig()

	if os.Getenv("KIONCTL_OFFLINE_DEV") == "1" {
		KEY_NAMESPACE = "_offline"
	}

	// Simplest transform function: put all the data files into the base dir.
	flatTransform := func(s string) []string { return []string{} }

	// Initialize a new cache store
	c.backend = diskv.New(diskv.Options{
		BasePath:     c.CacheDir,
		Transform:    flatTransform,
		CacheSizeMax: uint64(c.CacheSize),
		PathPerm:     0700,
		FilePerm:     0600,
	})

	c.Initialized = true
}

// TODO - add bits to allow cache expiry / TTL bits

// Get and set methods per type
func (c Cache) GetString(key string) (string, error) {
	if !c.Initialized {
		panic("Don't call methods before initializing")
	}

	val, err := c.backend.Read(key + "_str" + KEY_NAMESPACE)
	return string(val), err
}

func (c Cache) SetString(key string, value string) {
	if !c.Initialized {
		panic("Don't call methods before initializing")
	}

	c.backend.Write(key+"_str"+KEY_NAMESPACE, []byte(value))
}

func (c Cache) ExistsString(key string) bool {
	if !c.Initialized {
		panic("Don't call methods before initializing")
	}

	return c.backend.Has(key + "_str" + KEY_NAMESPACE)
}

func (c Cache) GetInt(key string) (int, error) {
	if !c.Initialized {
		panic("Don't call methods before initializing")
	}

	val, err := c.backend.Read(key + "_int" + KEY_NAMESPACE)
	val_int, _ := strconv.Atoi(string(val))
	return val_int, err
}

func (c Cache) SetInt(key string, value int) {
	if !c.Initialized {
		panic("Don't call methods before initializing")
	}

	c.backend.Write(key+"_int"+KEY_NAMESPACE, []byte(strconv.Itoa(value)))
}

func (c Cache) ExistsInt(key string) bool {
	if !c.Initialized {
		panic("Don't call methods before initializing")
	}

	return c.backend.Has(key + "_int" + KEY_NAMESPACE)
}

func (c Cache) DeleteAll() {
	if !c.Initialized {
		panic("Don't call methods before initializing")
	}

	c.backend.EraseAll()
}
