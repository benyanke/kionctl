package cmd

import (
	//"fmt"

	"fmt"

	"github.com/spf13/cobra"
	"gitlab.com/benyanke/kionctl/cache"
)

// listAccountsCmd represents the listAccounts command
var flushCacheCmd = &cobra.Command{
	Use:     "flush-cache",
	Example: "  kionctl flush-cache\n  kionctl flush",
	Short:   "Flush internal caches - you probably want fetch-info instead",
	Long: `For faster interaction, accounts and projects are cached
locally. Run this to look for new accounts if you have been granted new permissions.

Generally, you don't want to run this unless things are broken, as other useful data
is cached.

Typically, if you need to get updated account info, you should run:

 $ kionctl fetch-info
 
 `,
	Run: func(cmd *cobra.Command, args []string) {

		kionClient.IncrementStatistics("cache-flush", "", "", "")

		// Fetch statistics before flushing cache, and put back after clearing, to ensure
		// they aren't lost

		stats, _ := kionClient.Cache.GetString(cache.KEY_STATISTICS)
		kionClient.Cache.DeleteAll()
		kionClient.Cache.SetString(cache.KEY_STATISTICS, stats)

		fmt.Println("Local account detail caches flushed")
		fmt.Println("Run 'fetch-info' command to refresh them")
	},
}

func init() {
	rootCmd.AddCommand(flushCacheCmd)
}
