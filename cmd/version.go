package cmd

import (
	"encoding/json"
	"fmt"
	"io"
	"net/http"
	"os"
	"strings"

	"github.com/spf13/cobra"
)

type GitlabRelease struct {
	Tag string `json:"tag_name"`
}

// versionCmd represents the version command
var versionCmd = &cobra.Command{
	Use:     "version",
	Aliases: []string{"v"},
	Short:   "List current version information",

	Run: func(cmd *cobra.Command, args []string) {
		printVersionInfo()
		updateCheck()
	},
}

func init() {
	rootCmd.AddCommand(versionCmd)
}

// Prints version info to stdout
// Three cases to handle:
//  1. all var are empty. was built using "go build" during
//     development, or installed with go get (which just runs
//
// /   go build for the end user)
//  2. All are populated except GitTag. This is built by CI, on a non-release
//  3. All are populated. This is built by CI, for a release or release candidate.
//     3a) if the tag ends in "-rcX" where x is an integer, it's a release candidate
//     3a) if the tag matches vX.X.X where x is an integer, it's a release
func printVersionInfo() {

	if GitCommitHash == "" && GitCommitShortHash == "" && GitTag == "" {

		fmt.Println("No version available with the current build.")

	} else if GitCommitHash != "" && GitCommitShortHash != "" && GitTag == "" {

		fmt.Printf("Development build on commit %s\n", GitCommitShortHash)

		// I know the GitTag check is not valid, need to look into regex checking for this
		//} else if GitCommitHash != "" && GitCommitShortHash != "" && GitProject != "" && GitTag == "*-rcX" {
		//
		//	fmt.Printf("Pre-release build version %s, on commit %s\n", GitTag, GitCommitShortHash)

	} else if GitCommitHash != "" && GitCommitShortHash != "" && GitTag != "" {

		fmt.Printf("Release build version %s, on commit %s\n", GitTag, GitCommitShortHash)

	} else {
		panic("Unexpected state in version function - please report this error")
	}

	if GitProject != "" {
		fmt.Printf("Git repo URL: %s\n", GitProject)
	}

}

// Checks for updates by querying the git repo
func updateCheck() {

	// Disable update checks in offline mode
	if os.Getenv("KIONCTL_OFFLINE_DEV") == "1" {
		return
	}

	if ReleasesUrl != "" && GitTag != "" {

		releasesList, err := fetchURLContent(ReleasesUrl)
		latestRemoteVersion := parseLatestVersion(releasesList)

		if err == nil {
			if strings.TrimSpace(latestRemoteVersion) != strings.TrimSpace(GitTag) {
				fmt.Println("New version available! You're running '" + strings.TrimSpace(GitTag) + "' and '" + strings.TrimSpace(latestRemoteVersion) + "' is now available.")
			}
		}
	}

}

// Parses the gitlab releases api response and returns the git tag of the last release
func parseLatestVersion(allReleasesJson string) string {

	var releases []GitlabRelease
	json.Unmarshal([]byte(allReleasesJson), &releases)

	return releases[0].Tag

}

// Fetches content from the URL, returns it back to return string
func fetchURLContent(url string) (string, error) {
	resp, err := http.Get(url)
	if err != nil {
		return "", fmt.Errorf("GET error: %v", err)
	}
	defer resp.Body.Close()

	// TODO : Retry here on retryable errors
	if resp.StatusCode != http.StatusOK {
		return "", fmt.Errorf("status error: %v", resp.StatusCode)
	}

	data, err := io.ReadAll(resp.Body)
	if err != nil {
		return "", fmt.Errorf("read body: %v", err)
	}

	return string(data), nil

}
