package cmd

import (
	"log"
	"os"
	"path/filepath"
	"strconv"
	"strings"
	"time"

	"github.com/spf13/cobra"
	"github.com/spf13/viper"
	"gitlab.com/benyanke/kionctl/cache"
	"gitlab.com/benyanke/kionctl/kion"
	"gitlab.com/benyanke/kionctl/kion/api"
)

var cfgFile string

var kionClient kion.Client

// Injected at build time for things built in CI, but not for installs using 'go get'
var GitCommitHash string
var GitCommitShortHash string
var GitProject string
var GitTag string

type Account = api.Account
type AccessRole = api.AccessRole
type Project = api.Project
type ApiClient = api.Client
type Credential = api.Credential

// Gitlab releases API inserted here at buildtime
// Will be a value like: "https://gitlab.com/api/v4/projects/[project]/releases"
// Only works if the gitlab project is public, however
var ReleasesUrl string

// rootCmd represents the base command when called without any subcommands
var rootCmd = &cobra.Command{
	Use:   "kionctl",
	Short: "A CLI for Kion - formerly known as CloudTamer",
	Long: `This CLI tool helps you to make easier use of Kion (formerly known
as CloudTamer) at the command line, without needing to open a browser.

For more advanced usage instructions, run:
 $ kionctl usage
 
`,
	// Uncomment the following line if your bare application
	// has an action associated with it:
	// Run: func(cmd *cobra.Command, args []string) { },
	//PersistentPreRun: func(cmd *cobra.Command, args []string) {
	//
	//},
	//PersistentPostRun: func(cmd *cobra.Command, args []string) {
	//	log.Printf("Inside rootCmd PersistentPostRun with args: %v\n", args)
	//},
}

// Execute adds all child commands to the root command and sets flags appropriately.
// This is called by main.main(). It only needs to happen once to the rootCmd.
func Execute() {
	err := rootCmd.Execute()
	if err != nil {
		os.Exit(1)
	}
}

func init() {
	cobra.OnInitialize(initConfig)

	rootCmd.PersistentFlags().StringVar(&cfgFile, "config", "", "config file (default is $HOME/.config/kionctl/config.yaml)")
	rootCmd.PersistentFlags().BoolP("debug", "v", false, "Log more verbosely")
	rootCmd.PersistentFlags().BoolP("nocache", "n", false, "Disable reading from the cache - useful for intentionally reloading the cache")

	viper.BindPFlags(rootCmd.PersistentFlags())

}

// initConfig reads in config file and ENV variables if set.
func initConfig() {

	// If config flag is set, use it
	// if not, look in the normal places for config
	if cfgFile != "" {
		// Use config file from the flag.
		viper.SetConfigFile(cfgFile)
	} else {
		// look for config in $HOME/.config/kionctl/config.yaml
		viper.AddConfigPath("$HOME/.config/kionctl")
		// look for config in /etc/kionctl/config.yaml
		viper.AddConfigPath("/etc/kionctl")
		viper.SetConfigType("yaml")
		viper.SetConfigName("config")
	}

	viper.SetEnvPrefix("kionctl")
	viper.AutomaticEnv() // read in environment variables that match known config keys

	// If a config file is found, read it in.
	if err := viper.ReadInConfig(); err != nil {
		log.Println("Error parsing config: ", err)
		os.Exit(1)
	}

	// Set default config options
	viper.SetDefault("http_timeout", 30)
	viper.SetDefault("debug", false)
	viper.SetDefault("no_cache", false)

	viper.SetDefault("display.default_account_list_count", 15)

	viper.SetDefault("cache.dir", "~/.cache/kionctl")
	viper.SetDefault("cache.size", "1m")
	viper.SetDefault("aws-credential-file-path", "~/.aws/credentials")
	viper.SetDefault("aws-console-session-timeout", "3600")

	if !isStringAPositiveNumber(viper.GetString("aws-console-session-timeout")) {
		panic("aws-credential-file-path config option '" + viper.GetString("aws-console-session-timeout") + "' must be a positive integer")
	}

	if viper.GetBool("debug") {
		log.Println("Using config file:", viper.ConfigFileUsed())
	}

	// Mutate config values
	// Replace home paths like ~/.cache with a full real path
	viper.Set("cache.dir", expandHomedirPath(viper.GetString("cache.dir")))
	viper.Set("aws-credential-file-path", expandHomedirPath(viper.GetString("aws-credential-file-path")))

	// TODO - parse human readable cache size into raw byte count ( currently hardcoded to 1m )
	viper.Set("cache.size", 1024*1024)

	if viper.GetBool("nocache") {
		log.Println("Not reading from the cache, fetching everything new")
	}

	// TODO - maybe refactor this struct+init() into a constructor method returning a cache struct?
	cacheStruct := cache.Cache{
		CacheDir:  viper.GetString("cache.dir"),
		CacheSize: viper.GetInt("cache.size"),
	}
	cacheStruct.Init()

	// Bootstrap the kion client for all CLI invocations
	kionClient = kion.Client{
		BaseUrl:     viper.GetString("url"),
		ApiToken:    viper.GetString("api_key"),
		HttpTimeout: viper.GetInt("http_timeout"),
		Debug:       viper.GetBool("debug"),
		Cache:       cacheStruct,
		NoReadCache: viper.GetBool("nocache"),
	}
	kionClient.Init()

	// Ensure config as currently structured is valid
	kionClient.ExitOnValidationErrors()

	cacheStruct.SetInt(cache.KEY_LAST_RUNTIME, int(time.Now().Unix()))

}

// Expands homedir tilde strings, mainly from the config file
// expands "~/.config" to "/home/jdoe/.config", for example, but only if ~
// is the first character. Relies on the HOME env var
func expandHomedirPath(path string) string {

	homedir := strings.TrimSpace(os.Getenv("HOME"))
	path = strings.TrimSpace(path)

	if path == "~" {
		// If it's just the homedir itself, no further action needed
		return homedir
	} else if strings.HasPrefix(path, "~/") {
		// Use strings.HasPrefix so we don't match paths like
		// "/something/~/something/"
		path = filepath.Join(homedir, path[2:])
	} else {
		// If not a tilde path, still cleanup things like trailing slashes for
		// consistent usage later
		path = filepath.Join(path)
	}

	return strings.TrimSpace(path)

}

// Returns true if a string is a positive number, returns false if not. Also
// trims strings before attempting conversion
func isStringAPositiveNumber(input string) bool {

	val, err := strconv.Atoi(strings.TrimSpace(input))

	if err != nil {
		return false
	}

	if val > 0 {
		return true
	} else {
		return false
	}

}
