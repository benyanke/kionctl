package cmd

import (
	"os"
	"testing"
)

func TestIsStringAPositiveNumber(t *testing.T) {

	// Map inputs to expected outputs, test fails if the actual output
	// doesn't match the expected output
	test_pairs := map[string]bool{
		"1":   true,
		"-1":  false,
		" -1": false,
		"-1 ": false,

		"cow":              false,
		"one":              false,
		"longrandomstring": false,
		"":                 false,

		"10000":  true,
		" 10000": true,
		"10000 ": true,

		"-1030":  false,
		"534897": true,
		"42069":  true,

		"     -1030":  false,
		"   534897":   true,
		"  42069    ": true,

		"-1030    ":   false,
		"534897     ": true,
		"42069      ": true,
	}

	for input, expected_output := range test_pairs {

		actual_output := isStringAPositiveNumber(input)
		if actual_output != expected_output {
			t.Errorf("Input '%v' should generate output %v, instead it generated %v", input, expected_output, actual_output)
		}
	}

}

func TestExpandHomedirPath(t *testing.T) {

	home := "/home/testuser"
	os.Setenv("HOME", home)

	// Map inputs to expected outputs, test fails if the actual output
	// doesn't match the expected output
	test_pairs := map[string]string{
		// Test root case
		"~": home,

		// Test common cases
		"~/test":                                 home + "/test",
		"~/.cache/app":                           home + "/.cache/app",
		"~/.config/app":                          home + "/.config/app",
		"~/deep/nested/directory/structure/here": home + "/deep/nested/directory/structure/here",

		// Ensure leading/trailing spaces are trimmed, to fix weird environment var shell issues
		"~/.config/app ":         home + "/.config/app",
		" ~/.config/app":         home + "/.config/app",
		" ~/.config/app ":        home + "/.config/app",
		"     ~/.config/app    ": home + "/.config/app",
		" ~ ":                    home,
		" ~     ":                home,
		"~ ":                     home,
		" ~":                     home,

		// Ensure spaces in the middle of a path aren't modified
		"~/.config/ app/":            home + "/.config/ app",
		"~/te st/":                   home + "/te st",
		"~/.cache/app name/here":     home + "/.cache/app name/here",
		"~/.config/app name here/":   home + "/.config/app name here",
		"    ~/te st/":               home + "/te st",
		"~/.cache/app name/here    ": home + "/.cache/app name/here",

		// Ensure trailing slashes are never returned, even if in the original
		"~/.config/app/": home + "/.config/app",
		"~/directory/":   home + "/directory",

		// Ensure that ~ isn't replaced unless it's the start of the string
		"/root/var/lib/myapp/~/nested/tilde":      "/root/var/lib/myapp/~/nested/tilde",
		"/root/var/lib/myapp/~/nested/tilde/":     "/root/var/lib/myapp/~/nested/tilde",
		"   /root/var/lib/myapp/~/nested/tilde/ ": "/root/var/lib/myapp/~/nested/tilde",

		// Ensure that paths without ~ in them are not modified (except trailing slash)
		"/usr/local/bin/mytool": "/usr/local/bin/mytool",
		"/etc/kionctl":          "/etc/kionctl",

		"/root/very/long/path/here/ensure/not/modified":  "/root/very/long/path/here/ensure/not/modified",
		"/root/very/long/path/here/ensure/not/modified/": "/root/very/long/path/here/ensure/not/modified",
	}

	for input, expected_output := range test_pairs {

		actual_output := expandHomedirPath(input)
		if actual_output != expected_output {
			t.Errorf("Input '%q' should generate output %q, instead it generated %q", input, expected_output, actual_output)
		}
	}

}
