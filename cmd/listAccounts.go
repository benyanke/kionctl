package cmd

import (
	"fmt"
	"log"
	"os"
	"strconv"

	"github.com/spf13/cobra"
	"github.com/spf13/viper"
	"github.com/tomlazar/table"
)

type tableOutput struct {
	Project   string `table:"Project"`
	Account   string `table:"Account"`
	Role      string `table:"Role"`
	TimesUsed string `table:"Times Used"`
}

// listAccountsCmd represents the listAccounts command
var listAccountsCmd = &cobra.Command{
	Use:     "list-accounts",
	Aliases: []string{"list", "ls"},
	Short:   "List available accounts for the current user",
	Long:    `List the available AWS accounts that you have access to in Kion.`,
	Run: func(cmd *cobra.Command, args []string) {

		printAllAccounts, _ := cmd.Flags().GetBool("all")

		numberToPrint := viper.GetInt("display.default_account_list_count")

		// TODO - find a more elegant way to handle this
		if printAllAccounts {
			numberToPrint = 99999
		}

		projects, err := kionClient.GetAllAccountInformation()
		if err != nil {
			log.Println(err)
			os.Exit(1)
		}

		// intentionally doing nothing with the projects var, as this function's
		// primary purpose is warming local caches

		if kionClient.Debug {
			log.Println("Found " + strconv.Itoa(len(projects)) + " projects")
		}

		fmt.Println("")
		fmt.Println("You have the following projects, accounts and roles.")
		fmt.Println("")
		fmt.Println("If you don't see the expected data, try running with -n / --nocache and try again.")
		fmt.Println("")

		var tableOutputRows []tableOutput

		// Loop over projects, accounts in the project, and roles in the
		// account, and print every possible combination in the table
		for _, project := range projects {
			for _, account := range project.Accounts {
				for _, role := range account.AccessRoles {
					var row tableOutput
					row.Project = project.Name
					row.Account = account.Name + " (" + account.Number + ")"
					row.Role = role.AwsIamRoleName

					// Look up stats to populate the "TimesUsed" field
					count := kionClient.LookupStatsCountByAccountAndRole(account, role)
					row.TimesUsed = strconv.Itoa(count)

					tableOutputRows = append(tableOutputRows, row)

					numberToPrint--
					if numberToPrint <= 0 {
						break
					}
				}

				if numberToPrint <= 0 {
					break
				}
			}

			if numberToPrint <= 0 {
				break
			}
		}

		config := table.DefaultConfig()
		config.ShowIndex = false

		// Print table
		_ = table.MarshalTo(os.Stdout, tableOutputRows, config)

		// If numberToPrint is greater than zero, it means we printed all accounts without truncating any
		// If we *didn't* print all accounts, display a reminder that you can use -a to show them all
		if numberToPrint <= 0 {
			// TODO make go-vet happy and tell it to ignore the newlines
			fmt.Print("\nSome accounts were not shown due to number of accounts. To view all accounts, use -a / --all\n\n")
		}

	},
}

func init() {

	listAccountsCmd.Flags().BoolP("all", "a", false, "Print all accounts - don't truncate the account list")

	rootCmd.AddCommand(listAccountsCmd)

}
